package de.fzj.unicore.ucc.utils;

import de.fzj.unicore.uas.client.FileTransferClient;
import de.fzj.unicore.uas.client.FileTransferClient.IMonitorable;
import de.fzj.unicore.uas.client.StorageClient;
import de.fzj.unicore.uas.client.UFTPConstants;
import de.fzj.unicore.uas.client.UFTPFileTransferClient;
import de.fzj.unicore.ucc.MessageWriter;
import de.fzj.unicore.ucc.UCC;
import org.unigrids.services.atomic.types.GridFileType;
import org.unigrids.services.atomic.types.ProtocolType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

/**
 * helper that downloads remote files to the local client machine.<br/>
 * Simple wildcards ("*" and "?") and download of directories are supported.
 *
 * @author schuller
 */
public class FileDownloader extends FileTransferBase {

    public FileDownloader(String from, String to, Mode mode) {
        this(from, to, mode, true);
    }

    public FileDownloader(String from, String to, Mode mode, boolean failOnError) {
        this.to = to;
        this.from = from;
        this.mode = mode;
        this.failOnError = failOnError;
    }

    public void perform(StorageClient sms, MessageWriter msg) throws Exception {
        boolean isWildcard = hasWildCards(from);
        boolean isDirectory = false;
        GridFileType gridSource = null;
        if (isWildcard) {
            performWildCardExport(sms, msg);
        } else {
            //check if source is a directory
            gridSource = sms.listProperties(from);
            isDirectory = gridSource.getIsDirectory();
            if (isDirectory) {
                performDirectoryExport(gridSource, new File(to), sms, msg);
            } else {
                download(gridSource, new File(to), sms, msg);
            }
        }
    }

    protected void performDirectoryExport(GridFileType directory, File targetDirectory, StorageClient sms, MessageWriter msg) throws Exception {
        if (!targetDirectory.exists() || !targetDirectory.canWrite()) {
            throw new IOException("Target directory <" + to + "> does not exist or is not writable!");
        }
        if (!targetDirectory.isDirectory()) {
            throw new IOException("Target <" + to + "> is not a directory!");
        }
        GridFileType[] gridFiles = sms.listDirectory(directory.getPath());
        for (GridFileType file : gridFiles) {
            if (file.getIsDirectory()) {
                if (!recurse) {
                    msg.verbose("Skipping directory " + file.getPath());
                    continue;
                } else {
                    File newTargetDirectory = new File(targetDirectory, getName(file.getPath()));
                    boolean success = newTargetDirectory.mkdirs();
                    if (!success)
                        throw new IOException("Can create directory: " + newTargetDirectory.getAbsolutePath());
                    performDirectoryExport(file, newTargetDirectory, sms, msg);
                    continue;
                }
            }
            download(file, new File(targetDirectory, getName(file.getPath())), sms, msg);
        }
    }

    protected void performWildCardExport(StorageClient sms, MessageWriter msg) throws Exception {
        String dir = getDir(from);
        if (dir == null) dir = "/";
        GridFileType[] files = sms.find(dir, false, from, false, null, null);
        //local target must exist and be a directory
        File targetDir = new File(to);
        if (!targetDir.isDirectory()) throw new IOException("Target is not a directory.");
        for (GridFileType f : files) {
            download(f, targetDir, sms, msg);
        }
    }

    private String getDir(String path) {
        return new File(path).getParent();
    }

    private String getName(String path) {
        return new File(path).getName();
    }

    /**
     * download a single regular file
     *
     * @param source    - grid file descriptor
     * @param localFile - local file or directory to write to
     * @param msg
     * @param sms
     * @throws Exception
     */
    private void download(GridFileType source, File localFile, StorageClient sms, MessageWriter msg) throws Exception {
        if (source == null || source.getIsDirectory()) {
            throw new IllegalStateException("Source=" + source);
        }
        if (localFile.isDirectory()) {
            localFile = new File(localFile, getName(source.getPath()));
        }
        if (mode.equals(Mode.nooverwrite) && localFile.exists()) {
            msg.verbose("File exists and creation mode was set to 'nooverwrite'.");
            return;
        }
        FileOutputStream os = null;
        FileTransferClient ftc = null;
        try {
            String path = source.getPath();
            msg.verbose("Downloading remote file '" + sms.getUrl() + "#/" + path + "' -> " + localFile.getAbsolutePath());
            os = new FileOutputStream(localFile.getAbsolutePath(), mode.equals(Mode.append));
            chosenProtocol = sms.findSupportedProtocol(preferredProtocols.toArray(new ProtocolType.Enum[preferredProtocols.size()]));
            Map<String, String> extraParameters = makeExtraParameters(chosenProtocol, msg);
            ftc = sms.getExport(path, extraParameters, chosenProtocol);
            configure(ftc, extraParameters);
            msg.verbose("File transfer URL : " + ftc.getUrl());
            ProgressBar p = null;
            if (ftc instanceof IMonitorable) {
                long size = ftc.getSourceFileSize();
                p = new ProgressBar(localFile.getName(), size, msg);
                ((IMonitorable) ftc).setProgressListener(p);
            }
            long startTime = System.currentTimeMillis();
            ftc.readAllData(os);
            if (ftc instanceof IMonitorable) {
                p.finish();
            }
            if (timing) {
                long duration = System.currentTimeMillis() - startTime;
                double rate = (double) localFile.length() / (double) duration;
                msg.message("Rate: " + UCC.numberFormat.format(rate) + " kB/sec.");
            }
            copyProperties(source, localFile, msg);
        } finally {
            try {
                os.close();
            } catch (Exception ignored) {
            }
            if (ftc != null) {
                try {
                    ftc.destroy();
                } catch (Exception e1) {
                    msg.error("Could not destroy the filetransfer client", e1);
                }
            }
        }
    }

    /**
     * if possible, copy the remote executable flag to the local file
     *
     * @param targetFile - local file
     * @throws Exception
     */
    private void copyProperties(GridFileType source, File localFile, MessageWriter msg) throws Exception {
        try {
            localFile.setExecutable(source.getPermissions().getExecutable());
        } catch (Exception ex) {
            msg.error("Can't set 'executable' flag for " + localFile.getName(), ex);
        }
    }

    private void configure(FileTransferClient ftc, Map<String, String> params) {
        if (ftc instanceof UFTPFileTransferClient) {
            UFTPFileTransferClient u = (UFTPFileTransferClient) ftc;
            String secret = params.get(UFTPConstants.PARAM_SECRET);
            u.setSecret(secret);
        }
    }

}

