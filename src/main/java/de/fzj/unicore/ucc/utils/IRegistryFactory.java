package de.fzj.unicore.ucc.utils;

import de.fzj.unicore.ucc.MessageWriter;
import de.fzj.unicore.wsrflite.xmlbeans.client.IRegistryQuery;
import eu.unicore.security.util.client.IClientProperties;

/**
 * This class is intended to create IRegistryQuery instances
 *  
 * @author schuller
 */
public interface IRegistryFactory {

	/**
	 * create an IRegistryQuery instance connected to the given URL.
	 * If the URL does not denote a valid service this method MUST return null
	 * 
	 * @param url - the registry URL to connect to
	 * @param securityProperties - the client security properties
	 * @param msg - UCC message writer
	 * @return an {@link de.fzj.unicore.wsrflite.xmlbeans.client.IRegistryQuery} instance connected to the given URL, or <code>null</code> if
	 * the URL does not denote a valid service
	 */
	public IRegistryQuery create(String url, IClientProperties securityProperties, MessageWriter msg);
	
}
