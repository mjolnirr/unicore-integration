package de.fzj.unicore.ucc.utils;

import de.fzj.unicore.ucc.MessageWriter;
import de.fzj.unicore.wsrflite.xmlbeans.client.IRegistryQuery;
import de.fzj.unicore.wsrflite.xmlbeans.client.RegistryClient;
import eu.unicore.security.util.client.IClientProperties;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

public class U6RegistryFactory implements IRegistryFactory {

	@Override
	public IRegistryQuery create(String url,
			IClientProperties securityProperties, MessageWriter msg){
		
		if(url==null || (!url.contains("services/Registry"))){
			return null;
		}
		EndpointReferenceType epr=EndpointReferenceType.Factory.newInstance();
		epr.addNewAddress().setStringValue(url);
		try{
			return new RegistryClient(url,epr,securityProperties);
		}catch(Exception e){
			msg.error("Can't create client for <"+url+">", e);
			return null;
		}
	}

}
