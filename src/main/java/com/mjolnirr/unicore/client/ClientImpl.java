package com.mjolnirr.unicore.client;

import de.fzj.unicore.uas.TargetSystemFactory;
import de.fzj.unicore.uas.client.*;
import eu.emi.security.authn.x509.impl.KeystoreCertChainValidator;
import eu.emi.security.authn.x509.impl.KeystoreCredential;
import eu.emi.security.authn.x509.impl.ValidatorParamsExt;
import eu.unicore.security.canl.DefaultAuthnAndTrustConfiguration;
import eu.unicore.security.canl.IAuthnAndTrustConfiguration;
import eu.unicore.util.httpclient.ClientProperties;
import de.fzj.unicore.ucc.utils.FileDownloader;
import de.fzj.unicore.ucc.utils.FileUploader;
import de.fzj.unicore.ucc.utils.Mode;
import de.fzj.unicore.wsrflite.xmlbeans.WSUtilities;
import de.fzj.unicore.wsrflite.xmlbeans.client.RegistryClient;
import eu.unicore.util.httpclient.IClientConfiguration;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ApplicationDocument;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.ApplicationType;
import org.ggf.schemas.jsdl.x2005.x11.jsdl.JobDefinitionType;
import org.ggf.schemas.jsdl.x2005.x11.jsdlPosix.EnvironmentType;
import org.ggf.schemas.jsdl.x2005.x11.jsdlPosix.POSIXApplicationDocument;
import org.ggf.schemas.jsdl.x2005.x11.jsdlPosix.POSIXApplicationType;
import org.unigrids.x2006.x04.services.tss.ApplicationResourceType;
import org.unigrids.x2006.x04.services.tss.SubmitDocument;
import org.w3.x2005.x08.addressing.EndpointReferenceType;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStoreException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: zakharov
 * Date: 5/1/12
 * Time: 10:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClientImpl implements Client {
    private static final int MAX_EXECUTION_TIME = 43200;

    private RegistryClient registry;
    ExecutorService service = Executors.newFixedThreadPool(1);
    ScheduledExecutorService canceller = Executors.newSingleThreadScheduledExecutor();

    private String unicoreRegistryURL = "https://192.168.19.4:8080/REGISTRY/services/Registry?res=default_registry";

    public <T> Future<T> executeTask(Callable<T> c, long timeoutMS) {
        final Future<T> future = service.submit(c);

        canceller.schedule(new Callable<Void>() {
            public Void call() {
                future.cancel(true);
                return null;
            }
        }, timeoutMS, TimeUnit.SECONDS);

        return future;
    }

    @Override
    public void run(String dir, File configDir, String appName, String appVersion, Map<String, String> parameters, List<String> inputs, List<String> outputs) throws Exception {
        registry = initRegistryClient(configDir);

        final JobClient jobClient = createJob(appName, appVersion, parameters);
        StorageClient storageClient = jobClient.getUspaceClient();

        for (String input : inputs) {
            File directory = new File(dir);
            FileFilter fileFilter = new WildcardFileFilter(input);
            File[] realInputs = directory.listFiles(fileFilter);

            for (File realInput : realInputs) {
                FileUploader uploader = new FileUploader(realInput.getAbsolutePath(), "/" + realInput.getName(), Mode.overwrite);

                try {
                    uploader.perform(storageClient, new CAEMessageWriter());
                } catch (FileNotFoundException fnfe) {
//                    logger.error("Input file not found", fnfe);
//                    instanceLogger.error("Input file not found", fnfe);
                }
            }
        }

        jobClient.waitUntilReady(0);
        jobClient.start();
        executeTask(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                System.out.println("Wait until done");

                try {
                    jobClient.waitUntilDone(0);
                } catch (InterruptedException ie) {
                    System.out.println("Job aborted");

                    jobClient.abort();
                }

                System.out.println("Job finished or aborted");

                return null;
            }
        }, MAX_EXECUTION_TIME);

        try {
            jobClient.waitUntilDone(0);
        } catch (Exception e) {
            throw new TimeoutException("Work timed out");
        }

        for (String output : outputs) {
            FileDownloader downloader = new FileDownloader(output, dir, Mode.overwrite);

            try {
                downloader.perform(storageClient, new CAEMessageWriter());
            } catch (FileNotFoundException fnfe) {
                fnfe.printStackTrace();
//                instanceLogger.error("Output file not found", fnfe);
            }
        }
    }

    private IClientConfiguration setupSecurity(File configDir) throws IOException, KeyStoreException {
//        ClientProperties sp = new ClientProperties(new Properties());
//        sp.setProperty(ClientProperties.WSRF_SSL, "true");
//
//        Configuration conf = config.getConfig();
//
//        sp.setProperty(ClientProperties.WSRF_SSL_KEYSTORE, conf.getString(SSL_KEYSTORE));
//        sp.setProperty(ClientProperties.WSRF_SSL_KEYPASS, conf.getString(SSL_KEYPASS));
//        sp.setProperty(ClientProperties.WSRF_SSL_KEYALIAS, conf.getString(SSL_KEYALIAS));
//        sp.setProperty(ClientProperties.WSRF_SSL_TRUSTSTORE, conf.getString(SSL_TRUSTSTORE));
//        sp.setProperty(ClientProperties.WSRF_SSL_TRUSTPASS, conf.getString(SSL_TRUSTPASS));

        //SSL setup: use a JKS keystore + truststore

        String keystorePath = new File(configDir, "demouser.jks").getAbsolutePath().toString();
        char[] storePasswd = "the!user".toCharArray();
        char[] keyPasswd = storePasswd;
        String keyAlias = null; //auto-detected
        String storeType = "jks";
        KeystoreCredential cred = new KeystoreCredential(keystorePath, storePasswd, keyPasswd, keyAlias, storeType);

        KeystoreCertChainValidator validator = new KeystoreCertChainValidator(keystorePath, storePasswd, storeType, 60 * 1000, new ValidatorParamsExt());

        IAuthnAndTrustConfiguration sslConfig = new DefaultAuthnAndTrustConfiguration(validator, cred);
        ClientProperties secConfig = new ClientProperties(new Properties(), sslConfig);

        return secConfig;
    }

    private RegistryClient initRegistryClient(File configDir) throws Exception {
        String url = unicoreRegistryURL;
        EndpointReferenceType epr = EndpointReferenceType.Factory.newInstance();
        epr.addNewAddress().setStringValue(url);
        RegistryClient registryClient = new RegistryClient(url, epr, setupSecurity(configDir));

        return registryClient;
    }

    private TSSClient createTSSClient(String appName, RegistryClient registry) throws Exception {
        List<EndpointReferenceType> tsfEPRs = registry.listAccessibleServices(TargetSystemFactory.TSF_PORT);

        for (EndpointReferenceType epr : tsfEPRs) {
            String url = epr.getAddress().getStringValue();

            TSFClient tsf = new TSFClient(url, epr, registry.getSecurityConfiguration());

            List<EndpointReferenceType> availableTSS = tsf.getAccessibleTargetSystems();

            if (availableTSS.size() > 0) {
                for (EndpointReferenceType tss : availableTSS) {
                    TSSClient tssClient = new TSSClient(availableTSS.get(0), registry.getSecurityConfiguration());

                    List<ApplicationResourceType> availableApplications = tssClient.getApplications();

                    for (ApplicationResourceType app : availableApplications) {
                        if (appName.equalsIgnoreCase(app.getApplicationName())) {
                            return tssClient;
                        }
                    }
                }
            } else {
                TSSClient tssClient = tsf.createTSS();

                List<ApplicationResourceType> availableApplications = tssClient.getApplications();

                for (ApplicationResourceType app : availableApplications) {
                    if (appName.equalsIgnoreCase(app.getApplicationName())) {
                        return tssClient;
                    }
                }
            }
        }

        return null;
    }

    private JobClient createJob(String appName, String appVersion, Map<String, String> parameters) throws Exception {
        TSSClient targetSystem = createTSSClient(appName, registry);

        ApplicationDocument ad = ApplicationDocument.Factory.newInstance();
        ApplicationType app = ad.addNewApplication();

        SubmitDocument submitDoc = SubmitDocument.Factory.newInstance();
        JobDefinitionType job = submitDoc.addNewSubmit().addNewJobDefinition();
        app.setApplicationName(appName);
        app.setApplicationVersion(appVersion);

        POSIXApplicationDocument pd = POSIXApplicationDocument.Factory.newInstance();
        POSIXApplicationType p = pd.addNewPOSIXApplication();

        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            EnvironmentType et = p.addNewEnvironment();
            et.setName(entry.getKey());
            et.setStringValue(entry.getValue());
        }

        WSUtilities.append(pd, ad);

        job.addNewJobDescription().setApplication(app);

        JobClient jobClient = targetSystem.submit(submitDoc);

        return jobClient;
    }
}
