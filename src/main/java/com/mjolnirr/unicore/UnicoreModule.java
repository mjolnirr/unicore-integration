package com.mjolnirr.unicore;

import com.mjolnirr.lib.annotations.MjolnirrComponent;
import com.mjolnirr.lib.annotations.MjolnirrMethod;
import com.mjolnirr.lib.component.AbstractModule;
import com.mjolnirr.lib.component.ComponentContext;
import com.mjolnirr.unicore.client.Client;
import com.mjolnirr.unicore.client.ClientImpl;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sk_ on 3/10/14.
 */
@MjolnirrComponent(componentName = "unicore", instancesMaxCount = 1)
public class UnicoreModule extends AbstractModule {
    private ComponentContext context;

    @MjolnirrMethod(maximumExecutionTime = 9999)
    public List<File> run(String appName, 
                String appVersion, 
                Map<String, String> parameters, 
                List<File> inputFiles, 
                List<String> inputs, 
                List<String> outputs) throws Exception {
        Map<String, String> uploadedFiles = new HashMap<String, String>();
        int currentFileNumber = 0;

        for (File file : inputFiles) {
            FileUtils.copyFileToDirectory(file, getWorkspace());
            uploadedFiles.put("file" + currentFileNumber, file.getName());
            inputs.add(file.getName());

            currentFileNumber++;
        }

        for (String key : parameters.keySet()) {
            if (uploadedFiles.containsKey(parameters.get(key))) {
                parameters.put(key, uploadedFiles.get(parameters.get(key)));
            }
        }

        Client client = new ClientImpl();

        client.run(getWorkspace().getAbsolutePath().toString(), getConfigDir(), appName, appVersion, parameters, inputs, outputs);

        List<File> results = new ArrayList<File>();

        for (String output : outputs) {
            File directory = getWorkspace();
            FileFilter fileFilter = new WildcardFileFilter(output);
            File[] realInputs = directory.listFiles(fileFilter);

            for (File realInput : realInputs) {
                results.add(realInput);
            }
        }

        return results;
    }

    private File getConfigDir() {
        return context.getConfigDir();
//        return new File("/home/sk_/tmp/");
    }

    private File getWorkspace() {
        return context.getWorkspace();
//        return new File("/home/sk_/tmp/");
    }

    @Override
    public void initialize(ComponentContext componentContext) {
        this.context = componentContext;
    }
}
